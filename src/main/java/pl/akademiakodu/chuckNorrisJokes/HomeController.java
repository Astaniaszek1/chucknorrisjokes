package pl.akademiakodu.chuckNorrisJokes;

import guru.springframework.norris.chuck.ChuckNorrisQuotes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    private ChuckNorrisQuotes chuckNorrisQuotes = new ChuckNorrisQuotes();

    @GetMapping("/chuckNorris")
    public String jokes(ModelMap map) {
        map.put("jokes", chuckNorrisQuotes.getRandomQuote());
        return "chuckNorris";
    }

}
